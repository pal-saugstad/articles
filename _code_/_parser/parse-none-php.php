<?php

function parse_none_php($body_content, $echo_body) {

    # error_log ("-----> File ".__FILE__." Line ".__LINE__);
    $offset_body = 0;
    echo '<div style="padding: 20px;">';
    $basedir = dirname(dirname(__FILE__)).'/';
    if ($body_content == '_rst_') {
        require_once $basedir.'RST/autoload.php';
        $parser = new Gregwar\RST\Parser;
        echo $parser->parse($echo_body);
    } elseif ($body_content == '_md_') {
        require_once 'Parsedown.php';
        require_once 'ParsedownExtra.php';
        $parser = new Parsedown();
        echo $parser->text($echo_body);
    } else {
        echo "Format not known";
    }
    echo '</div>';
}
?>
