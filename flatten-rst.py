#!/usr/bin/python

import sys
import os
import re

def find_lang(filename_in):
    fh = open(filename_in)
    line = fh.readline()
    lang_arr = None
    m = re.search('^x \.\. multi (.+)$', line)
    if m:
        lang_arr = m.group(1).split(' ')
        print lang_arr
    fh.close()
    return lang_arr

def parse_out(dirname, basename, lang, fho, level):

        if lang:
            lcode, lang_prefix = lang.split('-')
            lang_match = "^(x|{}) (.*)$".format(lcode)
            print "=====-----===== lang ", lang, " langmatch ", lang_match

        fh = open(os.path.join(dirname, basename))
        for line in iter(fh):
            m = re.search('\.\. include\:\: (.+)', line)
            if m:
                fho.write('\n')
                sub_lang = find_lang(os.path.join(dirname, m.group(1)))
                parse_out (dirname, m.group(1), lang if sub_lang else None, fho, level+1)
                fho.write('\n')
            else:
                do_print = True
                if lang:
                    m = re.match(lang_match, line)
                    if m:
                        line = m.group(2)+'\n'
                    elif re.match('^x$', line):
                        line = '\n'
                    else:
                        do_print = False
                if do_print:
                    fho.write(line)

basename = os.path.basename(sys.argv[1])
dirname = os.path.dirname(sys.argv[1])
filename_out = sys.argv[2]

lang_match = ''
lang_arr = find_lang(sys.argv[1])

if lang_arr:
    for lang in lang_arr:
        _, file = lang.split('-')
        fho = open(os.path.join('tmp', file), 'w')
        parse_out (dirname, basename, lang, fho, 0)
        fho.close()
else:
    fho = open(filename_out, 'w')
    parse_out (dirname, basename, None, fho, 0)
    fho.close()
