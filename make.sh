#!/bin/bash

function must_make {
    doit='yes'
    if [ -e $2 ]; then
        [ "$(find $1 -type f -newer $2)" ] || doit=''
    fi
    echo "$doit"
}

is_older () {
    # return 0 if job should be run again
    [ ! -e $2 ] && return 0
    test $1 -nt $2
}

function finalize_revision {
    cat rst/revision-list.rst rst/revision-tail.rst >rst/revision-header.rst
    rm rst/revision-list.rst rst/revision-tail.rst
}

function generate_file_list {
    revfile=rst/revision-list.rst
    if [ "$1" == "filename" ]; then
        echo >$revfile
    fi
    printf ".. |$1| replace:: $2\n" >> $revfile
}

function generate_revision {

    hash=$(git log -1 --pretty=%h)
    version_revdatetime=$(git show -s --format=%ai HEAD)
    version_date=$(echo $version_revdatetime | awk '{print $1}')
    version_datetime=$version_revdatetime
    if [ "$(git status --porcelain)" ]; then
        hash="$hash-d"
        version_date=$(date --rfc-3339=date)
        version_datetime=$(date +"%Y-%m-%d %H:%M:%S %z Work In Progress")
        [ -e date ] && version_datetime="$version_datetime - with date override $(cat date)"
    elif [ -e date ] ; then
        version_date=$(cat date | awk '{print $1}')
        version_datetime=$(cat date)
    fi

    minor=$(git rev-list --count $(git log -1 --pretty=%H -- version)..HEAD)
    [ -e minor ] && minor=$(cat minor)
    revision=$(cat version).$minor-$hash
    if [ "$1" ]; then
        revfile=rst/revision-header.rst
        printf ".. |filename| unicode:: $1\n" > $revfile
    else
        revfile=rst/revision-tail.rst
        echo >$revfile
    fi
    printf ".. |author| replace:: $(cat author)\n" >> $revfile
    printf ".. |version| unicode:: $(cat version).$minor\n" >> $revfile
    printf ".. |revision| unicode:: $revision\n" >> $revfile
    printf ".. |revdatetime| unicode:: $version_revdatetime\n" >> $revfile
    printf ".. |datetime| unicode:: $version_datetime\n" >> $revfile
    printf ".. |date| unicode:: $version_date\n" >> $revfile

    revfile=rst/revision-header-hr.rst
    printf " - :Av: $(cat author)\n" > $revfile
    printf " - :Versjon: $(cat version).$minor\n" >> $revfile
    printf " - :Revisjon: $revision\n" >> $revfile
    printf " - :Dato: $version_date\n" >> $revfile
}

function to_html {
    FILENAME=$1
    if [ -n "$(must_make . ../parsed/"$FILENAME"/nor/src/revision-header.rst)" ]; then
        echo ============== "$FILENAME"
        generate_revision
        generate_file_list filename "$FILENAME".rst
        generate_revision_rst $hash $version_date
        ../flatten-rst.py rst/main.rst tmp~.rst
        cp tmp~.rst "$FILENAME".rst
        for subfile in $(find rst -name sub.*.rst); do
            coreval=${subfile:8:-4}
            corename=$(basename "$subfile")
            ../flatten-rst.py $subfile tmp-s~.rst
            cp tmp-s~.rst "$FILENAME".$corename
            generate_file_list sub_"$coreval" "$FILENAME"."$corename"
            cat tmp-s~.rst >>tmp~.rst
            rm tmp-s~.rst
        done
        rm rst/revision-header-hr.rst
        finalize_revision

        tdir=../parsed/"$FILENAME"
        mkdir -p "$tdir"/rev
        cp tmp~.rst "$tdir"/rev/$revision.rst
        mkdir -p "$tdir"/src
        mkdir -p "$tdir"/nor/src
        cp rst/revision-header.rst "$tdir"/nor/src
        mv rst/revision-header.rst "$tdir"/src
        mv "$FILENAME".* "$tdir"
    fi
}

function leave {
    echo $1
    exit 1
}

function to_pdf {
    FILENAME=$1
    for lang in nor ; do
        tdir=../parsed/"$FILENAME"/"$lang"
        F_RES="$tdir"/"$FILENAME".pdf
        if [ -n "$(must_make . $F_RES)" ]; then
            echo ============== "$F_RES"
            mkdir -p "$tdir"/src
            generate_revision "$FILENAME".pdf
            ../flatten-rst.py rst/main.rst tempfile1
            IFS=''
            cat tempfile1 | while read -r line ; do
              [ "$(echo $line | egrep '[a-z]\-$')" ] && echo -n "${line:0:-1}" || echo "$line"
            done > tempfile
            IFS=' '
            pandoc -f rst -o $F_RES tempfile -V papersize=$papersize $fontsize -V geometry:"$geometry" $toc $do_include
            [ "$?" -eq 0 ] || leave "Error pandoc didn't generate a pdf"
            counts=($(pandoc -f rst -t plain tempfile | wc))
            echo Innholdet i dokumentet "$F_RES" teller "${counts[2]}" bytes, dvs. "${counts[1]}" ord.

            mv rst/revision-header.rst "$tdir"/src
            rm rst/revision-header*
            rm tempfile
            rm tempfile1
        fi
    done
}

function to_direct_parsing {
    FILENAME=$1
    F_RES="../_code_/$FILENAME.rst"
    if [ -n "$(must_make . $F_RES)" ]; then
        echo ============== "$F_RES"
        cp rst/main.rst "$F_RES"
    fi
}

function to_plain {
    FILENAME="$1"
    F_RES="../_code_/$FILENAME"
    if [ -n "$(must_make . $F_RES)" ]; then
        echo ============== "$F_RES"
        for dotfile in $(find . -name *.dot 2>/dev/null); do
            out_file=../_code_/img/$(echo $(basename $dotfile) | sed 's/\.dot$/\.png/')
            is_older $dotfile $out_file
            if [ $? -eq 0 ] ; then
                echo Generate $out_file from $dotfile
                dot -Tpng $dotfile > $out_file
            fi
        done
        generate_revision "$FILENAME"
        ../flatten-rst.py rst/main.rst tempfile1
        ../substitute-rst.py tempfile1 tempfile2
        pandoc -f rst -t html -o "$F_RES" tempfile2
        rm tempfile*
        rm rst/revision-header*
    fi
}

function to_htm {
    to_plain "$1".htm
}

function to_php {
    to_plain "$1".php
    for in_file in $(find img -type f 2>/dev/null) ; do
        out_file=$(echo $in_file | sed 's/^/\.\.\/_code_\//g')
        is_older $in_file $out_file
        if [ $? -eq 0 ] ; then
            cp $in_file $out_file
            echo New img file $in_file copied to $out_file
        fi
    done
}

path="$(pwd)"

cd "$(realpath $(dirname $0))"

for convf in $(ls */converters); do
    DIR=$(dirname $convf)
    convf_override=${DIR}.converters
    source std_params
    [ -f ${DIR}/params ] && source ${DIR}/params
    [ -f $convf_override ] && CONV=$(cat $convf_override) || CONV=$(cat $convf)
    cd $DIR
    for RUN_ in $CONV ; do
        ${RUN_} $DIR
    done
    cd ..
done

cd "$path"
