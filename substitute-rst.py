#!/usr/bin/python

import sys
import re

def find_date(basename):
    fh = open(basename)
    for line in iter(fh):
        m = re.search('\.\. \|date\| \S* (.+)', line)
        if m:
            return m.group(1)
    return '__DATE__'

def parse_out(basename, fho, date):

    fh = open(basename)
    for line in iter(fh):
        line = re.sub(r'__DATE__', date, line)
        line = re.sub(r'twitter\.com\/intent\/tweet\?', 'twitter.com/intent/tweet?via=PalSaugstad&amp;', line)
        fho.write(line)

fm_name = sys.argv[1]
to_name = sys.argv[2]

date = find_date(fm_name)
# print 'Date is ', date

fho = open(to_name, 'w')
parse_out (fm_name, fho, date)
fho.close()
